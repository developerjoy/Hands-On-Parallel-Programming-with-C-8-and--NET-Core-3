﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Ch09
{
    public class _3AsyncPLinq
    {
        public static void Main()
        {
            Console.WriteLine($"MainThread({Thread.CurrentThread.ManagedThreadId})");
            ForEach();
            ParallelForEach();

            Console.ReadKey();
        }

        private static async void ForEach()
        {
            var urls = new List<string>
            {
                "https://www.google.com",
                "https://www.yahoo.com",
                "https://www.bing.com",
            };
            HttpClient client = new HttpClient();
            foreach (var url in urls)
            {
                HttpResponseMessage response = await client.GetAsync(url);
                string content = await response.Content.ReadAsStringAsync();
                Console.WriteLine($"ForEach, TaskID({Task.CurrentId}), ThreadID({Thread.CurrentThread.ManagedThreadId}), Got Url({url}) content({content.Length}).");
            }
        }

        private static void ParallelForEach()
        {
            var urls = new List<string>
            {
                "https://www.google.com",
                "https://www.yahoo.com",
                "https://www.bing.com",
            };

            HttpClient client = new HttpClient();

            Parallel.ForEach(urls, async url =>
            {
                HttpResponseMessage response = await client.GetAsync(url).ConfigureAwait(false);
                string content = await response.Content.ReadAsStringAsync();
                Console.WriteLine($"Parallel, TaskID({Task.CurrentId}), ThreadID({Thread.CurrentThread.ManagedThreadId}), Got Url({url}) content({content.Length}).");
            });
        }
    }
}
