﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Ch05
{
    public class _2Interlocked
    {
        public static void Main()
        {
            // RaceConditionIncrement();
            // InterlockedIncrement();
            testWithoutLock();
            testLock();
        }

        private static void InterlockedOperations()
        {
            //_counter becomes 1
            Interlocked.Increment(ref _counter);
            // _counter becomes 0
            Interlocked.Decrement(ref _counter);
            // Add: _counter becomes 2                                
            Interlocked.Add(ref _counter, 2);
            //Subtract: _counter becomes 0
            Interlocked.Add(ref _counter, -2);
            // Reads 64 bit field                                  
            Console.WriteLine(Interlocked.Read(ref _counter));
            // Swaps _counter value with 10       
            Console.WriteLine(Interlocked.Exchange(ref _counter, 10));
            //Checks if _counter is 10 and if yes replace with 100                                                       
            Console.WriteLine(Interlocked.CompareExchange(ref _counter, 100, 10)); // _counter becomes 100

            Interlocked.MemoryBarrier();

            Interlocked.MemoryBarrierProcessWide();
        }

        private static void InterlockedIncrement()
        {
            Parallel.For(1, 1000, i =>
            {
                Thread.Sleep(100);
                Interlocked.Increment(ref _counter);
            });
            Console.WriteLine($"Value for counter should be 999 and is {_counter}");
        }

        private static void testWithoutLock()
        {
            var counter = 0;
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            Parallel.For(1, 1000, i =>
            {
                Thread.Sleep(10);
                ++counter;
            });

            stopwatch.Stop();
            Console.WriteLine($"WithoutLock, count:{counter}, time:{stopwatch.Elapsed}");
        }

        private static readonly object Locker = new object();
        private static void testLock()
        {
            var counter = 0;
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            Parallel.For(1, 1000, i =>
            {
                Thread.Sleep(10);
                lock(Locker)
                    ++counter;
            });

            stopwatch.Stop();
            Console.WriteLine($"Lock, count:{counter}, time:{stopwatch.Elapsed}");
        }

        static long _counter;

        private static void RaceConditionIncrement()
        {
            Parallel.For(1, 1000, i =>
            {
                Thread.Sleep(100);
                _counter++;
            });
            Console.WriteLine($"Value for counter should be 999 and is {_counter}");
        }
    }
}