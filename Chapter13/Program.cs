﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ch13
{
    class Program
    {
        static void Main(string[] args)
        {
            // MapReduceTest();
            // AggregationTestSimple();
            // AggregationTestParallel();
            // AggregationTestConcurrent();
            // AggregationTestPLinq();
            // SpeculativeForEachTest();

            LazyDemo();
            // Console.ReadLine();
        }

        private static void LazyDemo()
        {
            Lazy<Task<string>> lazy = new Lazy<Task<string>>(Task.Factory.StartNew(GetDataFromService));
            lazy.Value.ContinueWith((s) => SaveToText(s.Result));
            lazy.Value.ContinueWith((s) => SaveToCsv(s.Result));
        }

        public static string GetDataFromService()
        {
            Console.WriteLine("Service called");
            return "Some Dummy Data";
        }

        public static void SaveToText(string data)
        {
            Console.WriteLine("Save to Text called");

            //Save to Text
        }

        public static void SaveToCsv(string data)
        {
            Console.WriteLine("Save to CSV called");

            //Save to CSV
        }

        private static void SpeculativeForEachTest()
        {
            string result = SpeculativeInvoke(square, square2);
            Console.WriteLine(result);

            string square()
            {
                Console.WriteLine("Square Called");
                return $"Result From Square is {5 * 5}";
            }

            string square2()
            {
                Console.WriteLine("Square2 Called");

                var square = 0;
                for(int j = 0; j < 5; j++)
                {
                    square += 5;
                }

                return $"Result From Square2 is {square}";
            }
        }

        public static T SpeculativeInvoke<T>(params Func<T>[] functions)
        {
            return SpeculativeForEach(functions, function => function());
        }

        public static TResult SpeculativeForEach<TSource, TResult>(
            IEnumerable<TSource> source, Func<TSource, TResult> body)
        {
            object result = null;
            Parallel.ForEach(source, (item, loopState) =>
            {
                result = body(item);
                loopState.Stop();
            });
            return (TResult)result;
        }

        private static void AggregationTestConcurrent()
        {
            var input = Enumerable.Range(1, 50);
            var output = new ConcurrentBag<int>();

            Parallel.ForEach(input, item =>
            {
                var result = action(item);
                output.Add(result);
            });

            int action(int i) => i * i;
        }

        private static List<int> AggregationTestPLinq()
        {
            var input = Enumerable.Range(1, 50);

            return input.AsParallel()
                .Select(action)
                .ToList();

            int action(int i) => i * i;
        }

        private static List<int> AggregationTestParallel()
        {
            var output = new List<int>();
            var input = Enumerable.Range(1, 50);

            Parallel.ForEach(input, item =>
            {
                var result = action(item);
                lock(output)
                    output.Add(result);
            });

            return output;

            int action(int i) => i * i;
        }

        private static List<int> AggregationTestSimple()
        {
            var output = new List<int>();
            var input = Enumerable.Range(1, 50);

            foreach(var item in input)
            {
                var result = action(item);
                output.Add(result);
            }

            return output;

            int action(int i) => i * i;
        }

        private static void MapReduceTest()
        {
            // Maps only positive number from list.
            // IEnumerable<int> mapPositiveNumbers(int number)
            // {
            //     IList<int> positiveNumbers = new List<int>();
            //     if(number > 0)
            //         positiveNumbers.Add(number);
            //     return positiveNumbers;
            // }

            IEnumerable<int> mapPositiveNumbers(int number)
            {
                if(number > 0)
                    yield return number;
            }

            // Group results together.
            int groupNumbers(int value) => value;

            //Reduce function that counts the occurence of each number
            IEnumerable<KeyValuePair<int, int>> reduceNumbers(IGrouping<int, int> grouping)
                => new[] {new KeyValuePair<int, int>(grouping.Key, grouping.Count())};

            // Generate a list of random numbers between -10 and 10
            IList<int> sourceData = new List<int>();
            var rand = new Random(DateTime.Now.Second);
            for(int i = 0; i < 1000; i++)
            {
                sourceData.Add(rand.Next(-10, 10));
            }

            // Use MapReduce function
            ParallelQuery<KeyValuePair<int,int>> result = sourceData.AsParallel().MapReduce(mapPositiveNumbers, groupNumbers, reduceNumbers);

            // process the results
            foreach(var item in result)
            {
                Console.WriteLine($"{item.Key} found {item.Value} times");
            }
        }
    }
}