﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Library;

namespace Ch02
{
    public class _11WaitingOnTasks
    {
        public static void Main()
        {
            // var normalWaitTask = Task.Factory.StartNew(() => Algorithm.Calculate("Normal", 4));
            //Blocks the current thread until task finishes.
            // normalWaitTask.Wait();

            // Console.WriteLine("Normal Task Finishes");

            // WaitAllDemo();
            // WhenAllDemo();
            // WaitAnyDemo();
            // WhenAnyDemo();
            // WhenAllWithExceptionDemo();
            WhenAnyWithExceptionDemo();

            Console.ReadLine();
        }

        private static void WhenAnyDemo()
        {
            Task taskA = Task.Factory.StartNew(() => Algorithm.Calculate("Q", 4));
            Task taskB = Task.Factory.StartNew(() => Algorithm.Calculate("Z", 2));
            Task.WhenAny(taskA, taskB);
            Console.WriteLine("[WhenAnyDemo] Calling method finishes");
        }

        private static async void WhenAnyWithExceptionDemo()
        {
            Task taskA = Task.Run(() => throw new NotImplementedException());
            Task taskB = Task.Run(() => Algorithm.Calculate("Z", 2));

            Console.WriteLine("[WhenAnyWithExceptionDemo] Start WhenAny.");

            await Task.WhenAny(taskA, taskB);

            Console.WriteLine("[WhenAnyWithExceptionDemo] Calling method finishes.");
        }

        private static void WhenAllDemo()
        {
            Task taskA = Task.Factory.StartNew(() => Algorithm.Calculate("C", 4));
            Task taskB = Task.Factory.StartNew(() => Algorithm.Calculate("D", 2));
            Task.WhenAll(taskA, taskB);
            Console.WriteLine("[WhenAllDemo] Calling method finishes");
        }

        private static async void WhenAllWithExceptionDemo()
        {
            Task taskA = Task.Run(() => throw new NotImplementedException());
            Task taskB = Task.Factory.StartNew(() => Algorithm.Calculate("D", 2));

            Console.WriteLine("[WhenAllWithExceptionDemo] Start WhenAll.");
            try
            {
                await Task.WhenAll(taskA, taskB);
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }

            Console.WriteLine("[WhenAllWithExceptionDemo] Calling method finishes");
        }

        private static void WaitAnyDemo()
        {
            Task taskA = Task.Factory.StartNew(() => Algorithm.Calculate("E", 4));
            Task taskB = Task.Factory.StartNew(() => Algorithm.Calculate("F", 2));
            Task.WaitAny(taskA, taskB);
            Console.WriteLine("[WaitAnyDemo] Calling method finishes");
        }

        private static void WaitAllDemo()
        {
            Task taskA = Task.Factory.StartNew(() => Algorithm.Calculate("A", 4));
            Task taskB = Task.Factory.StartNew(() => Algorithm.Calculate("B", 2));
            Task.WaitAll(taskA, taskB);
            Console.WriteLine("[WaitAllDemo] Calling method finishes");
        }

        public async Task<string> DownloadStringWithTimeout(HttpClient client, string uri)
        {
            using var cts = new CancellationTokenSource(TimeSpan.FromSeconds(3));
            Task<string> downloadTask = client.GetStringAsync(uri);
            var timeoutTask = Task.Delay(Timeout.InfiniteTimeSpan, cts.Token);
            Task completedTask = await Task.WhenAny(downloadTask, timeoutTask);
            if (completedTask == timeoutTask)
                return null;
            return await downloadTask;
        }

        private bool mCanBehaveSynchronously;
        public ValueTask<int> MethodAsync()
        {
            if(mCanBehaveSynchronously)
                return new ValueTask<int>(13);
            return new ValueTask<int>(SlowMethodAsync());
        }

        private async Task<int> SlowMethodAsync()
        {
            return 100;
        }

        async Task ConsumingMethodAsync()
        {
            int value = await MethodAsync();
        }

        async Task ConsumingMethodAsync2()
        {
            ValueTask<int> valueTask = MethodAsync();
            // do other work.
            int value = await valueTask;
        }

        async IAsyncEnumerable<int> GetValuesAsync()
        {
            await Task.Delay(1000); // some asynchronous work.
            yield return 10;

            await Task.Delay(1000); // more asynchronous work.
            yield return 13;
        }

        async Task ProcessValueAsync(HttpClient client)
        {
            await foreach(string value in GetValuesAsync2(client))
            {
                Console.WriteLine(value);
            }
        }

        async IAsyncEnumerable<string> GetValuesAsync2(HttpClient client)
        {
            int offset = 0;
            const int limit = 10;
            while(true)
            {
                // Get the current page of results and parse them.
                string result = await client.GetStringAsync($"https://example.com/api/values?offset={offset}&limit={limit}");
                var valuesOnThisPage = result.Split('\n');

                // Produce the results for this page.
                foreach(string value in valuesOnThisPage)
                {
                    yield return value;
                }

                // If this is the last page, we're done.
                if(valuesOnThisPage.Length != limit)
                    break;

                // Otherwise, proceed to the next page.
                offset += limit;
            }
        }

        async Task ProcessValueAsync2(HttpClient client)
        {
            await foreach(string value in GetValuesAsync2(client).ConfigureAwait(false))
            {
                await Task.Delay(100).ConfigureAwait(false); // asynchronous work.
                Console.WriteLine(value);
            }
        }
    }

    interface IMyAsync
    {
        Task GetValueAsync();
    }

    class MySynchronousImpl : IMyAsync
    {
        public Task GetValueAsync()
        {
            return Task.CompletedTask;
        }
    }

    interface IMyAsyncWithValue
    {
        Task<int> GetValueAsync();
    }

    class MySyncImplWithValue : IMyAsyncWithValue
    {
        public Task<int> GetValueAsync()
        {
            return Task.FromException<int>(new NotImplementedException());
        }
    }

    interface IMyAsyncWithCancel
    {
        Task<int> GetValueAsync(CancellationToken token);
    }

    class MyCancel : IMyAsyncWithCancel
    {
        public Task<int> GetValueAsync(CancellationToken token)
        {
            if(token.IsCancellationRequested)
                return Task.FromCanceled<int>(token);
            return Task.FromResult(11);
        }

        private static readonly Task<int> ZeroTask = Task.FromResult(0);
    }
}
