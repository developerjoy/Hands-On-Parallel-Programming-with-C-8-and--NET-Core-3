﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace Ch02
{
    public class _10ParentChildTasks
    {
        public static void Main()
        {
            // DetachedTask();

            // AttachedTasks();


            var testAsync = new TestAsync();
            testAsync.DoSomething();

            Console.ReadLine();
        }

        private static void AttachedTasks()
        {
            Task parentTask = Task.Factory.StartNew(() =>
            {
                Console.WriteLine("     Parent task started");

                Task childTask = Task.Factory.StartNew(() =>
                    Console.WriteLine("     Child task started")
                ,TaskCreationOptions.AttachedToParent);
                Console.WriteLine("     Parent task Finish");
            });

            //Wait for parent to finish
            parentTask.Wait();
            Console.WriteLine("     Work Finished");
        }

        private static void DetachedTask()
        {
            Task parentTask = Task.Factory.StartNew(() =>
            {
                Console.WriteLine("     Parent task started");
                Task childTask = Task.Factory.StartNew(() => Console.WriteLine("     Child task started"));
                Console.WriteLine("     Parent task Finish");
            });
            //Wait for parent to finish
            parentTask.Wait();
            Console.WriteLine("     Work Finished");
        }
    }

    public class TestAsync
    {
        public async void DoSomething()
        {
            // IAsyncEnumerable<int> values = SlowRange().WhereAwait(async value =>
            IAsyncEnumerable<int> values = SlowRange().Where(value =>
            {
                // Do some asynchronous work to determine if this element should be included.
                Task.Delay(10);
                return value % 2 == 0;
            });

            await foreach(int result in values)
            {
                Console.WriteLine(result);
            }
        }

        /// <summary>
        /// Produce sequence that slows down as it progresses.
        /// </summary>
        /// <returns></returns>
        public async IAsyncEnumerable<int> SlowRange()
        {
            for(int i = 0; i != 10; i++)
            {
                await Task.Delay(i * 100);
                yield return i;
            }
        }
    }

    public class TestAsyncBreak
    {
        public async void DoSomething()
        {
            IAsyncEnumerable<int> values = SlowRange();

            await foreach(int result in values)
            {
                Console.WriteLine(result);
                if(result >= 8)
                    break;
            }
        }

        /// <summary>
        /// Produce sequence that slows down as it progresses.
        /// </summary>
        /// <returns></returns>
        public async IAsyncEnumerable<int> SlowRange()
        {
            for(int i = 0; i != 10; i++)
            {
                await Task.Delay(i * 100);
                yield return i;
            }
        }
    }

    public class TestAsyncCancellation
    {
        public async void DoSomething()
        {
            using var cts = new CancellationTokenSource(500);
            CancellationToken token = cts.Token;

            IAsyncEnumerable<int> values = SlowRange(token);

            await foreach(int result in values)
            {
                Console.WriteLine(result);
                if(result >= 8)
                    break;
            }
        }

        /// <summary>
        /// Produce sequence that slows down as it progresses.
        /// </summary>
        /// <returns></returns>
        public async IAsyncEnumerable<int> SlowRange([EnumeratorCancellation] CancellationToken token = default)
        {
            for(int i = 0; i != 10; i++)
            {
                await Task.Delay(i * 100, token);
                yield return i;
            }
        }
    }
}
