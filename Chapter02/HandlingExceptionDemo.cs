﻿using System;
using System.Threading.Tasks;

namespace Ch02
{
    public static class HandlingExceptionDemo
    {
        public static async void Main()
        {
            // await handleExceptionSelfAsync();

            // try
            // {
            //     await doNotHandleExceptionAsync();
            // }
            // catch(Exception e)
            // {
            //     Console.WriteLine(e);
            // }

            try
            {
                await Task.Run(() => throw new NotSupportedException());
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private static async Task handleExceptionSelfAsync()
        {
            try
            {
                await PossibleExceptionAsync();
            }
            catch(NotSupportedException e)
            {
                Console.WriteLine(e);
            }
        }

        private static async Task doNotHandleExceptionAsync()
        {
            await PossibleExceptionAsync();
        }

        private static Task PossibleExceptionAsync()
        {
            throw new NotSupportedException();
        }
    }
}