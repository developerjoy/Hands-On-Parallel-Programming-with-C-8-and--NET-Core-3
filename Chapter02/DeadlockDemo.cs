﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace Ch02
{
    public static class DeadlockDemo
    {
        public static async void Main()
        {
            var synchronizationContext = new MySynchronizationContext();
            SynchronizationContext.SetSynchronizationContext(synchronizationContext);

            Console.WriteLine($"Main Start..., ThreadID:{Thread.CurrentThread.ManagedThreadId }");

            var task = waitAsync();
            task.Wait();

            Console.WriteLine($"Main over..., ThreadID:{Thread.CurrentThread.ManagedThreadId }");
        }

        private static async Task waitAsync()
        {
            Console.WriteLine($"wait something start..., ThreadID:{Thread.CurrentThread.ManagedThreadId }");

            await Task.Delay(TimeSpan.FromSeconds(1));

            Console.WriteLine($"wait something over..., ThreadID:{Thread.CurrentThread.ManagedThreadId }");
        }
    }

    public class MySynchronizationContext : SynchronizationContext
    {
        private class Code
        {
            public SendOrPostCallback Action;
            public object? State;

            public void Execute()
            {
                Action(State);
            }
        }

        private readonly Thread mThread;
        private readonly ConcurrentQueue<Code> mQueue = new ConcurrentQueue<Code>();

        public MySynchronizationContext()
        {
            mThread = new Thread(onThreadUpdate);
        }

        private void onThreadUpdate()
        {
            while(true)
            {
                if(mQueue.Count == 0)
                {
                    Thread.Sleep(1000);
                    continue;
                }

                if(mQueue.TryDequeue(out var code))
                    code.Execute();
                else
                    Thread.Sleep(1000);
            }
        }

        public override void Post(SendOrPostCallback d, object? state)
        {
            mQueue.Enqueue(new Code {Action = d, State = state});
        }
    }
}