﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Randomizer;

namespace Library
{
    public static class Algorithm
    {
        public static int Calculate(string id, int count, CancellationToken? token = null)
        {
            int totalTime = 0;
            var random = new RandomIntegerGenerator();
            for(int i = 1; i <= count; i++)
            {
                int time = random.GenerateValue(900, 1100);
                Thread.Sleep(time);
                totalTime += time;
                Console.WriteLine($"[{id}] Computing... {i}/{count}, time:{time}, TaskID:{Task.CurrentId}");

                if(token.HasValue && token.Value.IsCancellationRequested)
                {
                    Console.WriteLine($"[{id}] Cancel..., TaskID:{Task.CurrentId}");
                    break;
                }
            }
            Console.WriteLine($"[{id}] Computing done, total time:{totalTime}, TaskID:{Task.CurrentId}");

            return totalTime;
        }
    }
}