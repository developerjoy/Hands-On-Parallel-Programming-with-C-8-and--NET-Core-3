﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Ch04
{
    public class _1AsParallel
    {
        public static void Main()
        {
            // sequential();
            // parallel();
            // parallelWithOrdered();

            plinqDemo();
        }

        private static void plinqDemo()
        {
            // Using LINQ
            IEnumerable<int> values = Enumerable.Range(1, int.MaxValue);
            var result = values.Where(x => x % 12345678 == 0).Select(x => x);
            Stopwatch sq = Stopwatch.StartNew();
            long sum = 0;
            foreach(var item in result)
                sum += item;
            Console.WriteLine($"Time taken for processing: {sq.Elapsed}");

            // Using PLINQ
            var result1 = values.AsParallel().Where(x => x % 12345678 == 0).Select(x => x);
            sq.Restart();
            sum = 0;
            foreach(var item in result1)
                sum += item;
            Console.WriteLine($"Time taken for processing: {sq.Elapsed}");
        }

        private static void sequential()
        {
            var range = Enumerable.Range(1, int.MaxValue);

            //Here is sequential version
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var result = range.Where(i => i % 3 == 0).ToList();
            stopwatch.Stop();
            Console.WriteLine($"Sequential: Count:{result.Count}, Time:{stopwatch.Elapsed}");
        }

        private static void parallel()
        {
            var range = Enumerable.Range(1, int.MaxValue);
            //Here is Parallel Version using .AsParallel method
            var stopwatch = Stopwatch.StartNew();
            var result = range.AsParallel().Where(i => i % 3 == 0).ToList();
            stopwatch.Stop();
            Console.WriteLine($"Parallel: Count:{result.Count}, Time:{stopwatch.Elapsed}");

            // resultList = (from i in range.AsParallel()
            //               where i % 3 == 0
            //               select i).ToList();
            //
            // Console.WriteLine($"Parallel: Total items are {resultList.Count}");
        }

        private static void parallelWithOrdered()
        {
            var range = Enumerable.Range(1, 9999999);
            var successResultList = range.AsParallel().Select(i => i % 3 == 0).ToList();
            var failedRequests = range.AsParallel().AsOrdered().Where((r, index) =>
            {
                Console.WriteLine($"Task id is {Task.CurrentId}" );
                return !successResultList[index];
            }
            ).ToList();
        }
    }
}
