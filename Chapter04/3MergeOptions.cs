﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Ch04
{
    public class _3MergeOptions
    {
        public static void Main()
        {
            NotBuffered();
            AutoBuffered();
            FullyBuffered();
        }

        private static void NotBuffered()
        {
            Console.WriteLine("========================Not Buffered====================================");
            var range = ParallelEnumerable.Range(1, 100);
            ParallelQuery<int> notBufferedQuery = range
                .WithMergeOptions(ParallelMergeOptions.NotBuffered)
                .Where(i => i % 10 == 0)
                .Select(x => x);
            var watch = Stopwatch.StartNew();
            foreach(var item in notBufferedQuery)
                Console.WriteLine($"ThreadID:{Thread.CurrentThread.ManagedThreadId}, {item}:{watch.ElapsedMilliseconds} ms");

            Console.WriteLine($"NotBuffered Full Result returned in {watch.ElapsedMilliseconds} ms.");
            watch.Stop();
        }

        private static void FullyBuffered()
        {
            Console.WriteLine("========================Full Buffered====================================");
            var range = ParallelEnumerable.Range(1, 100);
            ParallelQuery<int> fullyBufferedQuery = range
                .WithMergeOptions(ParallelMergeOptions.FullyBuffered)
                .Where(i => i % 10 == 0)
                .Select(x => x);

            var watch = Stopwatch.StartNew();
            foreach(var item in fullyBufferedQuery)
                Console.WriteLine($"ThreadID:{Thread.CurrentThread.ManagedThreadId}, {item}:{watch.ElapsedMilliseconds} ms");

            Console.WriteLine($"FullyBuffered Full Result returned in {watch.ElapsedMilliseconds} ms.");
            watch.Stop();
        }

        private static void AutoBuffered()
        {
            Console.WriteLine("========================Auto Buffered====================================");
            var range = ParallelEnumerable.Range(1, 100);
            ParallelQuery<int> query = range
                .WithMergeOptions(ParallelMergeOptions.AutoBuffered)
                .Where(i => i % 10 == 0)
                .Select(x => x);

            var watch = Stopwatch.StartNew();
            foreach(var item in query)
                Console.WriteLine($"ThreadID:{Thread.CurrentThread.ManagedThreadId}, {item}:{watch.ElapsedMilliseconds} ms");

            Console.WriteLine($"AutoBuffered Full Result returned in {watch.ElapsedMilliseconds} ms.");
            watch.Stop();
        }
    }
}