﻿//
// An orderable dynamic partitioner for lists
//

using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;

class OrderableListPartitioner<TSource> : OrderablePartitioner<TSource>
{
    private readonly IList<TSource> mInput;

    // Must override to return true.
    public override bool SupportsDynamicPartitions => true;

    public OrderableListPartitioner(IList<TSource> input) : base(true, false, true) => mInput = input;

    public override IList<IEnumerator<KeyValuePair<long, TSource>>> GetOrderablePartitions(int partitionCount)
    {
        var dynamicPartitions = GetOrderableDynamicPartitions();
        var partitions = new IEnumerator<KeyValuePair<long, TSource>>[partitionCount];

        for (int i = 0; i < partitionCount; i++)
        {
            partitions[i] = dynamicPartitions.GetEnumerator();
        }
        return partitions;
    }

    public override IEnumerable<KeyValuePair<long, TSource>> GetOrderableDynamicPartitions() => new ListDynamicPartitions(mInput);

    private class ListDynamicPartitions : IEnumerable<KeyValuePair<long, TSource>>
    {
        private readonly IList<TSource> mInput;
        private int mPos;

        internal ListDynamicPartitions(IList<TSource> input) => mInput = input;

        public IEnumerator<KeyValuePair<long, TSource>> GetEnumerator()
        {
            while (true)
            {
                // Each task gets the next item in the list. The index is incremented in a thread-safe manner to avoid races.
                int elemIndex = Interlocked.Increment(ref mPos) - 1;

                if (elemIndex >= mInput.Count)
                    yield break;

                yield return new KeyValuePair<long, TSource>(elemIndex, mInput[elemIndex]);
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable<KeyValuePair<long, TSource>>)this).GetEnumerator();
    }
}