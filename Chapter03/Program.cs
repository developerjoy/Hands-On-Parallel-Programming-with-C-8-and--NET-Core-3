﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Ch03
{
    public class Program
    {
        public static void Main()
        {
            var maxValue = 900000000;
            sequenceCompute(maxValue);
            parallelForCompute(maxValue);
            parallelForEachCompute(maxValue);
        }

        private static void sequenceCompute(int maxValue)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            long sum = 0;
            for(int i = 1; i <= maxValue; i++)
            {
                sum += i;
            }

            stopwatch.Stop();

            Console.WriteLine($"Sum:{sum}, Total Time:{stopwatch.Elapsed.TotalSeconds}");
        }

        private static void parallelForCompute(int maxValue)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            long total = 0;
            var taskNum = Environment.ProcessorCount;
            Parallel.For<long>(0, taskNum, () => 0, calculate, onFinish);

            long calculate(int index, ParallelLoopState state, long localSum)
            {
                var interval = maxValue / taskNum;

                var start = interval * index + 1;
                var end = interval * index + interval;

                Console.WriteLine($"TaskID:{Task.CurrentId}, Iteration:{index}, Range:{start} ~ {end}");

                for(int i = start; i <= end; i++)
                {
                    localSum += i;
                }

                return localSum;
            }

            void onFinish(long iterationValue)
            {
                Interlocked.Add(ref total, iterationValue);
            }

            stopwatch.Stop();
            Console.WriteLine($"Sum:{total}, Total Time:{stopwatch.Elapsed.TotalSeconds}");
        }

        private static void parallelForEachCompute(int maxValue)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var taskNum = Environment.ProcessorCount;
            var numbers = Enumerable.Range(0, taskNum);

            long total = 0;
            Parallel.ForEach(numbers, () => 0L, calculate, onFinish);

            long calculate(int index, ParallelLoopState state, long localSum)
            {
                var interval = maxValue / taskNum;

                var start = interval * index + 1;
                var end = interval * index + interval;

                Console.WriteLine($"TaskID:{Task.CurrentId}, Iteration:{index}, Range:{start} ~ {end}");

                for(int i = start; i <= end; i++)
                {
                    localSum += i;
                }

                return localSum;
            }

            void onFinish(long iterationValue)
            {
                Interlocked.Add(ref total, iterationValue);
            }

            stopwatch.Stop();
            Console.WriteLine($"Sum:{total}, Total Time:{stopwatch.Elapsed.TotalSeconds}");
        }

        // private static void parallelForEachPartitionerCompute(int maxValue)
        // {
        //     var stopwatch = new Stopwatch();
        //     stopwatch.Start();
        //
        //     var partitioner = Partitioner.Create(1, maxValue, 10000);
        //
        //     long total = 0;
        //     Parallel.ForEach(partitioner, );
        //
        //     long calculate(int index, ParallelLoopState state, long localSum)
        //     {
        //         var interval = maxValue / 5;
        //
        //         var start = interval * index + 1;
        //         var end = interval * index + interval;
        //         Console.WriteLine($"Iteration:{index}, Range:{start} ~ {end}");
        //         for(int i = start; i <= end; i++)
        //         {
        //             localSum += i;
        //         }
        //
        //         return localSum;
        //     }
        //
        //     void onFinish(long iterationValue)
        //     {
        //         Interlocked.Add(ref total, iterationValue);
        //     }
        //
        //     stopwatch.Stop();
        //     Console.WriteLine($"Sum:{total}, Total Time:{stopwatch.Elapsed.TotalSeconds}");
        // }
    }
}
