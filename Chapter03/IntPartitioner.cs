﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PartitionerDemo
{
    public class IntPartitioner : Partitioner<IntPartitioner.Range>
    {
        public class Range
        {
            public int Start;
            public int End;

            public override string ToString()
            {
                return $"{Start} ~ {End}";
            }
        }

        private const int Interval = 10;

        private readonly BlockingCollection<Range> mRanges = new BlockingCollection<Range>();

        public IntPartitioner(int min, int max)
        {
            for(int value = min; value <= max; value += Interval)
            {
                mRanges.Add(new Range {Start = value, End = value + Interval - 1});
            }
        }

        public override IList<IEnumerator<Range>> GetPartitions(int partitionCount)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<Range> GetDynamicPartitions()
        {
            Console.WriteLine($"TaskID:{Task.CurrentId}");
            yield return mRanges.Take();
        }

        public override bool SupportsDynamicPartitions => true;
    }
}