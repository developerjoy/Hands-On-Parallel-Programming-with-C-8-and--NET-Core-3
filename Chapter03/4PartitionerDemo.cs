﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using PartitionerDemo;

namespace Ch03
{
    public class _4PartitionerDemo
    {
        public static void Main()
        {
            // partitionerDemo();
            // customPartitionerDemo();
            // customOrderablePartitionerDemo2();
            customPartitionerDemo3();

            // demo();
        }

        private static void demo()
        {
            // Our sample collection
            string[] collection =
            {
                "red", "orange", "yellow", "green", "blue", "indigo",
                "violet", "black", "white", "grey"
            };

            // Instantiate a partitioner for our collection
            var myPart = new SingleElementPartitioner<string>(collection);

            //
            // Simple test with ForEach
            //
            // Console.WriteLine("Testing with Parallel.ForEach");
            // Parallel.ForEach(myPart,
            // item => { Console.WriteLine("  item = {0}, thread id = {1}", item, Thread.CurrentThread.ManagedThreadId); });

            //
            //
            // Demonstrate the use of static partitioning, which really means
            // "using a static number of partitioners".  The partitioners themselves
            // may still be "dynamic" in the sense that their outputs may not be
            // deterministic.
            //
            //

            // Perform static partitioning of collection
            IList<IEnumerator<string>> staticPartitions = myPart.GetPartitions(2);
            int index = 0;

            Console.WriteLine("Static Partitioning, 2 partitions, 2 tasks:");

            // Action will consume from static partitions
            Action staticAction = () =>
            {
                int myIndex = Interlocked.Increment(ref index) - 1; // compute your index
                IEnumerator<string> myItems = staticPartitions[myIndex]; // grab your static partition
                // int id = Thread.CurrentThread.ManagedThreadId; // cache your thread id

                // Enumerate through your static partition
                while(myItems.MoveNext())
                {
                    Thread.Sleep(50); // guarantees that multiple threads have a chance to run
                    Console.WriteLine("  item = {0}, thread id = {1}", myItems.Current,
                        Thread.CurrentThread.ManagedThreadId);
                }

                myItems.Dispose();
            };

            // Spawn off 2 actions to consume 2 static partitions
            Parallel.Invoke(staticAction, staticAction);

            //
            //
            // Demonstrate the use of dynamic partitioning
            //
            //

            // Grab an IEnumerable which can then be used to generate multiple
            // shared IEnumerables.
            var dynamicPartitions = myPart.GetDynamicPartitions();

            Console.WriteLine("Dynamic Partitioning, 3 tasks:");

            // Action will consume from dynamic partitions
            Action dynamicAction = () =>
            {
                // Grab an enumerator from the dynamic partitions
                var enumerator = dynamicPartitions.GetEnumerator();
                int id = Thread.CurrentThread.ManagedThreadId; // cache our thread id

                // Enumerate through your dynamic enumerator
                while(enumerator.MoveNext())
                {
                    Thread.Sleep(50); // guarantees that multiple threads will have a chance to run
                    Console.WriteLine("  item = {0}, thread id = {1}", enumerator.Current, id);
                }

                enumerator.Dispose();
            };

            // Spawn 3 concurrent actions to consume the dynamic partitions
            Parallel.Invoke(dynamicAction, dynamicAction, dynamicAction);

            // Clean up
            if(dynamicPartitions is IDisposable)
                ((IDisposable)dynamicPartitions).Dispose();
        }

        private static void partitionerDemo()
        {
            // var source = Enumerable.Range(1, 100).ToList();
            // var partitioner = Partitioner.Create(1, 100, 30);
            // var partitioner = Partitioner.Create(1, 100);
            // IEnumerable<int> data = Enumerable.Range(0, 10);
            List<int> data = Enumerable.Range(0, 10).ToList();
            var partitioner = Partitioner.Create(data);

            // Parallel.ForEach(partitioner, (range, state) =>
            // {
            //     var startRange = range.Item1;
            //     var endRange = range.Item2;
            //     Console.WriteLine($"Range execution finished on task {Task.CurrentId} with range {startRange}-{endRange}");
            // });

            Parallel.ForEach(partitioner,
                (value, state) => { Console.WriteLine($"TaskID:{Task.CurrentId}, value:{value}"); });
        }

        private static void customPartitionerDemo()
        {
            int sum = 0;
            for(int value = 1; value <= 100; value++)
            {
                sum += value;
            }

            Console.WriteLine($"Sync Sum = {sum}");

            sum = 0;
            var intPartitioner = new IntPartitioner(1, 100);
            Parallel.ForEach(intPartitioner, range =>
            {
                Console.WriteLine($"TaskID={Task.CurrentId}, Range: {range.Start} ~ {range.End}");
                for(int value = range.Start; value <= range.End; value++)
                {
                    Interlocked.Add(ref sum, value);
                }
            });

            Console.WriteLine($"Parallel Sum = {sum}");
        }

        private static void customOrderablePartitionerDemo2()
        {
            var nums = Enumerable.Range(0, 10000).ToArray();
            OrderableListPartitioner<int> partitioner = new OrderableListPartitioner<int>(nums);

            // Use with Parallel.ForEach
            Parallel.ForEach(partitioner, (i) => Console.WriteLine(i));

            // Use with PLINQ
            var query = from num in partitioner.AsParallel()
                where num % 2 == 0
                select num;

            foreach(var v in query)
                Console.WriteLine(v);
        }

        private static void customPartitionerDemo3()
        {
            var source = Enumerable.Range(0, 10000).ToArray();

            Stopwatch sw = Stopwatch.StartNew();
            MyPartitioner partitioner = new MyPartitioner(source, .5);


            var query = from n in partitioner.AsParallel()
                select ProcessData(n);


            foreach(var v in query)
            {
            }

            Console.WriteLine("Processing time with custom partitioner {0}", sw.ElapsedMilliseconds);


            var source2 = Enumerable.Range(0, 10000).ToArray();


            sw = Stopwatch.StartNew();


            var query2 = from n in source2.AsParallel()
                select ProcessData(n);


            foreach(var v in query2)
            {
            }

            Console.WriteLine("Processing time with default partitioner {0}", sw.ElapsedMilliseconds);
        }

        // Consistent processing time for measurement purposes.
        static int ProcessData(int i)
        {
            Thread.SpinWait(i * 1000);
            return i;
        }
    }
}