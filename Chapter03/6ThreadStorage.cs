﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Ch03
{
    public class _6ThreadStorage
    {
        public static void Main()
        {
            ThreadLocalVariableForEach();
            // ThreadLocalVariableFor();
            // Console.ReadLine();
        }

        private static void ThreadLocalVariableFor()
        {
            var numbers = Enumerable.Range(1, 60);
            long sumOfNumbers = 0;

            // collection of 60 number with each number having value equal to index
            Parallel.For(0, numbers.Count(),
                defaultThreadLocal, calculate, onFinish
            );

            Console.WriteLine($"The total of 60 numbers is {sumOfNumbers}");

            // method to initialize the local variable
            static long defaultThreadLocal() => 0;

            // Action performed on each iteration
            static long calculate(int index, ParallelLoopState state, long subtotal)
            {
                var oldSubTotal = subtotal;
                subtotal += index; //Subtotal is Thread local variable

                Console.WriteLine($"Calculate, Task:{Task.CurrentId}, SubTotal:{oldSubTotal} -> {subtotal}, Index:{index}");
                return subtotal; // value to be passed to next iteration
            }

            void onFinish(long taskResult)
            {
                var oldSum = sumOfNumbers;
                Interlocked.Add(ref sumOfNumbers, taskResult);
                Console.WriteLine($"Finish, Task:{Task.CurrentId}, Sum:{oldSum} -> {sumOfNumbers}, SubTotal:{taskResult}");
            }
        }

        private static void ThreadLocalVariableForEach()
        {
            var numbers = Enumerable.Range(1, 60);
            long sumOfNumbers = 0;

            // collection of 60 number with each number having value equal to index
            Parallel.ForEach(numbers, defaultThreadLocal, calculate, onFinish);

            // method to initialize the local variable
            static long defaultThreadLocal() => 0;

            // Action performed on each iteration
            static long calculate(int index, ParallelLoopState state, long subtotal)
            {
                var oldSubTotal = subtotal;
                subtotal += index; //Subtotal is Thread local variable

                Console.WriteLine($"Calculate, Task:{Task.CurrentId}, SubTotal:{oldSubTotal} -> {subtotal}, Index:{index}");
                return subtotal; // value to be passed to next iteration
            }

            void onFinish(long taskResult)
            {
                var oldSum = sumOfNumbers;
                Interlocked.Add(ref sumOfNumbers, taskResult);
                Console.WriteLine($"Finish, Task:{Task.CurrentId}, Sum:{oldSum} -> {sumOfNumbers}, SubTotal:{taskResult}");
            }

            Console.WriteLine($"The total of 60 numbers is {sumOfNumbers}");
        }
    }
}