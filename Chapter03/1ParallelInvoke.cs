﻿using System;
using System.Threading.Tasks;
using Library;

namespace Ch03
{
    public class _1ParallelInvoke
    {
        public static void Main()
        {
            try
            {
                // Parallel.Invoke(() => Console.WriteLine("Action 1"),
                                // () => Console.WriteLine("Action 2"));

                Parallel.Invoke(() => Algorithm.Calculate("AA", 3),
                                () => Algorithm.Calculate("BB", 3));
            }
            catch(AggregateException aggregateException)
            {
                foreach (var ex in aggregateException.InnerExceptions)
                {
                    Console.WriteLine(ex.Message);
                }
            }
           
            Console.WriteLine("Unblocked");
            Console.ReadLine();
           
            Task.Factory.StartNew(
                () => {
                    Task.Factory.StartNew(() => Console.WriteLine("Action 1"),
                        TaskCreationOptions.AttachedToParent);
                    Task.Factory.StartNew(new Action(() => Console.WriteLine("Action 2"))
                        , TaskCreationOptions.AttachedToParent);
                        }
                );
            
        }
    }
}
