﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Library;

namespace Ch03
{
    public class _7ParallelLoops
    {
        public static void Main()
        {
            ParallelFor();
            ParallelForEach();
            Console.ReadLine();
        }

        private static void ParallelForEach()
        {
            // var urls = new List<string>() {"www.google.com" , "www.yahoo.com","www.bing.com" };
            // Parallel.ForEach(urls, url =>
            // {
            //     Ping ping = new Ping();
            //     Console.WriteLine($"Ping Url {url} status is {ping.Send(url).Status} by Task {Task.CurrentId}");
            // });
            //
            // Console.WriteLine("after Parallel.ForEach....");

            int totalTime = 0;
            var counts = new List<int> {2, 3, 5, 1, 4};
            ParallelLoopResult r = Parallel.ForEach(counts, count =>
            {
                var time = Algorithm.Calculate(count.ToString(), count);
                Console.WriteLine($"[{count}] Algorithm time = {time}");
                Interlocked.Add(ref totalTime, time);
            });

            Console.WriteLine($"IsCompleted:{r.IsCompleted}, LowestBreakIteration:{r.LowestBreakIteration}");
            Console.WriteLine($"Total Time = {totalTime}");
        }

        private static void ParallelFor()
        {
            // int totalFiles = 0;
            // var files = Directory.GetFiles("C:\\");
            // Parallel.For(0, files.Length, (i) =>
            //      {
            //          FileInfo fileInfo = new FileInfo(files[i]);
            //          if (fileInfo.CreationTime.Day == DateTime.Now.Day)
            //              Interlocked.Increment(ref totalFiles);
            //      });
            // Console.WriteLine($"Total number of files in C: drive are {files.Count()} and  {totalFiles} files were created today.");

            int totalTime = 0;
            ParallelLoopResult r = Parallel.For(1, 6, count =>
            {
                var time = Algorithm.Calculate(count.ToString(), count);
                Console.WriteLine($"[{count}] Algorithm time = {time}");
                Interlocked.Add(ref totalTime, time);
            });

            Console.WriteLine($"IsCompleted:{r.IsCompleted}, LowestBreakIteration:{r.LowestBreakIteration}");
            Console.WriteLine($"Total Time = {totalTime}");
        }
    }
}
