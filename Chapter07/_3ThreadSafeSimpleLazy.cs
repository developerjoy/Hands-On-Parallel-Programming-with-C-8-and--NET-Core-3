﻿using System.Threading;

namespace Ch07
{
    class _3ThreadSafeSimpleLazy
    {
        Data _cachedData;

        static object _locker = new object();

        public Data GetOrCreate()
        {
            // Try to load cached data.
            var data = _cachedData;

            // If data not created yet.
            if (data == null)
            {
                // Lock the shared resource.
                lock (_locker)
                {
                    // Second try to load data from cache as it might have been populate
                    // by another thread while current thread was waiting for lock.
                    data = _cachedData;

                    // If Data not cached yet.
                    if (data == null)
                    {
                        // Load data from database and cache for later use.
                        data = GetDataFromDatabase();
                        _cachedData = data;
                    }
                }
            }
            return _cachedData;
        }

        private Data GetDataFromDatabase()
        {
            //Dummy Delay
            Thread.Sleep(5000);
            return new Data();
        }

        public void ResetCache()
        {
            _cachedData = null;
        }
    }
}
