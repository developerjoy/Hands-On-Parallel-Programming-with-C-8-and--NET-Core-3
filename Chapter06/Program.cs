﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Ch06
{
    public class Program
    {
        public static void Main()
        {
            // ProducerConsumerUsingQueues();
            // ProducerConsumerUsingQueuesWithLock();
            // ProducerConsumerUsingConcurrentQueues();
            // ProducerConsumerUsingConcurrentStack();

            // ConcurrentBackDemo();
            // ConcurrentBackDemo2();
            // BlockingCollectionDemo();
            // BlockingCollectionMultipleProducerConsumer();
            // ConcurrentDictionaryDemo();

            testTaskTerminated();

            Console.ReadLine();
        }

        private static void testTaskTerminated()
        {
            var testObj = new TestObj();
        }

        private static void BlockingCollectionMultipleProducerConsumer()
        {
            var produceCollections = new BlockingCollection<int>[2];
            produceCollections[0] = new BlockingCollection<int>(5);
            produceCollections[1] = new BlockingCollection<int>(5);

            Task producerTask1 = Task.Factory.StartNew(() =>
            {
                for(int i = 1; i <= 5; ++i)
                {
                    produceCollections[0].Add(i);
                    Thread.Sleep(100);
                }

                produceCollections[0].CompleteAdding();
            });

            Task producerTask2 = Task.Factory.StartNew(() =>
            {
                for(int i = 6; i <= 10; ++i)
                {
                    produceCollections[1].Add(i);
                    Thread.Sleep(200);
                }

                produceCollections[1].CompleteAdding();
            });

            while(!produceCollections[0].IsCompleted || !produceCollections[1].IsCompleted)
            {
                int item;
                BlockingCollection<int>.TryTakeFromAny(produceCollections, out item, TimeSpan.FromSeconds(1));
                if(item != default(int))
                {
                    Console.WriteLine($"Fetched item is {item}");
                }
            }
        }

        private static void BlockingCollectionDemo()
        {
            var blockingCollection = new BlockingCollection<int>(10);

            Task producerTask = Task.Factory.StartNew(() =>
            {
                for(int i = 0; i < 5; ++i)
                {
                    Console.WriteLine($"{DateTime.Now}, Producer add item:{i}");
                    blockingCollection.Add(i);
                }

                blockingCollection.CompleteAdding();
            });

            Task consumerTask = Task.Factory.StartNew(() =>
            {
                while(!blockingCollection.IsCompleted)
                {
                    int item = blockingCollection.Take();
                    Console.WriteLine($"{DateTime.Now}, Consumer received item is {item}");
                }

                Console.WriteLine($"{DateTime.Now}, Consumer BlockingCollection is CompleteAdding...");
            });

            Task.WaitAll(producerTask, consumerTask);
        }

        private static void ConcurrentDictionaryDemo()
        {
            var concurrentDictionary = new ConcurrentDictionary<int, string>();

            Task producerTask1 = Task.Factory.StartNew(() =>
            {
                for(int i = 0; i < 20; i++)
                {
                    Thread.Sleep(100);
                    concurrentDictionary.TryAdd(i, (i * i).ToString());
                }
            });
            Task producerTask2 = Task.Factory.StartNew(() =>
            {
                for(int i = 10; i < 25; i++)
                {
                    Thread.Sleep(100);
                    concurrentDictionary.TryAdd(i, (i * i).ToString());
                }
            });
            Task producerTask3 = Task.Factory.StartNew(() =>
            {
                for(int i = 15; i < 20; i++)
                {
                    Thread.Sleep(100);
                    concurrentDictionary.AddOrUpdate(i, (i * i).ToString(), (key, value) => (key * key).ToString());
                }
            });

            Task.WaitAll(producerTask1, producerTask2);

            Console.WriteLine(
                $"Keys are {string.Join(", ", concurrentDictionary.Keys.Select(c => c.ToString()).ToArray())} ");
        }

        static ConcurrentBag<int> concurrentBag = new ConcurrentBag<int>();

        private static void ConcurrentBackDemo()
        {
            var manualResetEvent = new ManualResetEventSlim(false);

            Task producerAndConsumerTask = Task.Factory.StartNew(() =>
            {
                for(int i = 1; i <= 3; ++i)
                {
                    concurrentBag.Add(i);
                }

                //Allow second thread to add items
                manualResetEvent.Wait();

                while(concurrentBag.IsEmpty == false)
                {
                    if(concurrentBag.TryTake(out var item))
                    {
                        Console.WriteLine($"Item is {item}");
                    }
                }
            });


            Task producerTask = Task.Factory.StartNew(() =>
            {
                for(int i = 4; i <= 6; ++i)
                {
                    concurrentBag.Add(i);
                }

                manualResetEvent.Set();
            });

            Console.ReadLine();

            //  Task.WaitAll(producerAndConsumerTask, producerTask);
        }

        private static void ConcurrentBackDemo2()
        {
            var manualResetEvent = new ManualResetEventSlim(false);

            Task producer = Task.Factory.StartNew(() =>
            {
                for(int i = 1; i <= 3; ++i)
                {
                    concurrentBag.Add(i);
                }

                manualResetEvent.Set();
            });

            Task producerTask = Task.Factory.StartNew(() =>
            {
                for(int i = 4; i <= 6; ++i)
                {
                    concurrentBag.Add(i);
                }

                manualResetEvent.Wait();

                while(concurrentBag.IsEmpty == false)
                {
                    if(concurrentBag.TryTake(out var item))
                    {
                        Console.WriteLine($"Item is {item}");
                    }
                }
            });

            Console.ReadLine();

            //  Task.WaitAll(producerAndConsumerTask, producerTask);
        }

        static Queue<int> _queue = new Queue<int>();
        static object _locker = new object();

        private static void ProducerConsumerUsingQueues()
        {
            // Create a Queue.
            var queue = new Queue<int>();

            // Populate the queue.
            for(int i = 0; i < 500; i++) queue.Enqueue(i);

            int sum = 0;

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            Parallel.For(0, 500, (i) =>
            {
                int localSum = 0;
                while(queue.TryDequeue(out var localValue))
                {
                    Thread.Sleep(10);
                    localSum += localValue;
                }

                Interlocked.Add(ref sum, localSum);
            });

            stopwatch.Stop();

            Console.WriteLine($"Calculated Sum is {sum} and actual sum should be {Enumerable.Range(0, 500).Sum()}");
            Console.WriteLine($"Time:{stopwatch.Elapsed}");
        }

        private static void ProducerConsumerUsingQueuesWithLock()
        {
            // Create a Queue.
            Queue<int> cq = new Queue<int>();

            // Populate the queue.
            for(int i = 0; i < 500; i++) cq.Enqueue(i);

            int sum = 0;

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            Parallel.For(0, 500, (i) =>
            {
                int localSum = 0;
                Monitor.Enter(_locker);
                while(cq.TryDequeue(out var localValue))
                {
                    Thread.Sleep(10);
                    localSum += localValue;
                }

                Monitor.Exit(_locker);
                Interlocked.Add(ref sum, localSum);
            });

            stopwatch.Stop();

            Console.WriteLine($"Calculated Sum is {sum} and actual sum should be {Enumerable.Range(0, 500).Sum()}");
            Console.WriteLine($"Time:{stopwatch.Elapsed}");
        }

        private static void ProducerConsumerUsingConcurrentQueues()
        {
            // Create a Queue.
            var queue = new ConcurrentQueue<int>();

            // Populate the queue.
            for(int i = 0; i < 500; i++) queue.Enqueue(i);

            int sum = 0;

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            Parallel.For(0, 500, (i) =>
            {
                int localSum = 0;
                while(queue.TryDequeue(out var localValue))
                {
                    Thread.Sleep(10);
                    localSum += localValue;
                }

                Interlocked.Add(ref sum, localSum);
            });

            stopwatch.Stop();

            Console.WriteLine($"outerSum = {sum}, actual sum should be {Enumerable.Range(0, 500).Sum()}");
            Console.WriteLine($"Time:{stopwatch.Elapsed}");
        }

        private static void ProducerConsumerUsingConcurrentStack()
        {
            // Create a Queue.
            var stack = new ConcurrentStack<int>();

            // Populate the queue.
            for(int i = 0; i < 500; i++) stack.Push(i);
            stack.PushRange(new[] {1, 2, 3, 4, 5});

            int sum = 0;

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            Parallel.For(0, 500, (i) =>
            {
                int localSum = 0;
                while(stack.TryPop(out var localValue))
                {
                    Thread.Sleep(10);
                    localSum += localValue;
                }

                Interlocked.Add(ref sum, localSum);
            });

            stopwatch.Stop();

            Console.WriteLine($"outerSum = {sum}, and actual sum should be 124765");
            Console.WriteLine($"Time:{stopwatch.Elapsed}");
        }
    }
}