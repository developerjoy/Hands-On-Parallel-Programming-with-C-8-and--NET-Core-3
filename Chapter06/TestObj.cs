﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace Ch06
{
    public class TestObj
    {
        private readonly int mMaxValue;
        public TestObj(int maxValue = 15)
        {
            mMaxValue = maxValue;
            Task.Run(produceValue);
            Task.Run(consumeValue);
        }

        private readonly ConcurrentQueue<int> mQueue = new ConcurrentQueue<int>();
        private void produceValue()
        {
            for(int i = 0; i < mMaxValue; i++)
            {
                mQueue.Enqueue(i);
                Thread.Sleep(1000);
            }

            Console.WriteLine("produceValue terminated.");
        }

        private void consumeValue()
        {
            while(true)
            {
                if(mQueue.TryDequeue(out var value))
                    Console.WriteLine($"{DateTime.Now}, Consume {value}.");
                else
                    Thread.Sleep(500);
            }
        }
    }
}