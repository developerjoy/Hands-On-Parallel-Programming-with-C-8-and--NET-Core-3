﻿
using System.Threading.Tasks;
using Ch02;

namespace MyConsole
{
    static class Program
    {
        static async Task Main(string[] args)
        {
            // Chapter 2.
            // _1CreatingAndStartingTasks.Main();
            // _3CancelingTasks.Main();
            // _4HandlingExceptionFromSingleTask.Main();
            // _5HandlingExceptionFromMultipleTasks.Main();
            // _7APMToTask.Main();
            // HandlingExceptionDemo.Main();
            // DeadlockDemo.Main();
            // _11WaitingOnTasks.Main();
            _10ParentChildTasks.Main();

            // chapter 3.
            // _1ParallelInvoke.Main();
            // _3DegreeOfParallelism.Main();
            // _4PartitionerDemo.Main();
            // _5CancellingLoop.Main();
            // _6ThreadStorage.Main();
            // _7ParallelLoops.Main();
            // Ch03.Program.Main();

            // chapter 4.
            // _1AsParallel.Main();
            // _2Ordering.Main();
            // _3MergeOptions.Main();
            // _4ExceptionHandling.Main();
            // _5CombiningSequential.Main();
            // _6CancellingPlinq.Main();

            // chapter 5.
            // _2Interlocked.Main();
            // _4SignalingPrimitives.Main();
            // _5SlimLocks.Main();

            // chapter 6.
            // Ch06.Program.Main();

            // chapter 7.
            // Ch07.DataWrapper.Main();
            // Ch07._5LazyUsingDelegate.Main();
            // Ch07._6_1_ExceptionsWithLazyWithCaching.Main();
            // Ch07._6_2_ExceptionsWithLazyWithCaching.Main();
            // Ch07._7ThreadLocal.Main();
            // Ch07._7_2ThreadLocal.Main();
            // Ch07._8_LazyInitializer.Main();

            // chapter 9.
            // Ch09._2ExceptionHandling.Main();
            // Ch09._3AsyncPLinq.Main();
            // await Ch09._4AsyncPerformance.Main();
            // await Ch09._4AsyncPerformance.MainAsync();
        }
    }
}