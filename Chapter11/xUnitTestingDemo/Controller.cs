﻿using System;
using System.Threading.Tasks;

namespace UnitTestingDemo
{
    public interface IService
    {
        Task<string> GetDataAsync();
    }

    class Controller
    {
        public Controller(IService service)
        {
            Service = service;
        }

        public IService Service { get; }

        public async Task DisplayData()
        {
            var data = await Service.GetDataAsync();
            Console.WriteLine(data);
        }
    }
}