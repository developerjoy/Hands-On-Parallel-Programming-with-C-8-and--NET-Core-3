﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace UnitTestingDemo
{
    public class UnitTestForException
    {
        private async Task<float> GetDivisionAsync(int number , int divisor)
        {
            if (divisor == 0)
            {
                throw new DivideByZeroException();
            }

            int result = await Task.Run(() =>
            {
                Thread.Sleep(1000);
                return number / divisor;
            });

            return result;
        }
        [Fact]
        public async void GetDivisionAsyncShouldReturnSuccessIfDivisorIsNotZero()
        {
            int number = 20;
            int divisor = 4;
            var result = await GetDivisionAsync(number, divisor);

            Assert.Equal(result, 5);
        }

        [Fact]
        public void GetDivisionAsyncShouldCheckForExceptionIfDivisorIsNotZero()
        {
            int number = 20;
            int divisor = 0;
            Assert.ThrowsAsync<DivideByZeroException>(async () => await GetDivisionAsync(number, divisor));
        }
    }
}