﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Ch10
{
    public class Program
    {
        public static void Main()
        {
            var task1 = Task.Run(() =>
            {
                Thread.Sleep(1500);
                Console.WriteLine($"ThreadID = {Thread.CurrentThread.ManagedThreadId}");
            });

            var task2 = Task.Run(() =>
            {
                Thread.Sleep(3000);
                Console.WriteLine($"ThreadID = {Thread.CurrentThread.ManagedThreadId}");
            });

            for (int i = 1; i <= 10; i++)
            {
                Task task = new Task(() =>
                 {
                     for(int j = 0; j < 100; j++)
                         Thread.Sleep(100);

                     Console.WriteLine($"ThreadID = {Thread.CurrentThread.ManagedThreadId}");
                 });
                task.Start();
            }
            
            // Action computeAction = () =>
            // {
            //     int i = 0;
            //     while (true)
            //     {
            //         i = 1 * 1;
            //     }
            // };
            // Task.Run(() => computeAction());
            // Task.Run(() => computeAction());
            // Task.Run(() => computeAction());
            // Task.Run(() => computeAction());
            Console.ReadLine();
        }
    }
}
